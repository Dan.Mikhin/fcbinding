using System;
using Foundation;
using ObjCRuntime;
using UIKit;

namespace FreshchatSDK
{
	// @interface FreshchatConfig : NSObject
	[BaseType (typeof(NSObject))]
	interface FreshchatConfig
	{
		// @property (nonatomic, strong) NSString * appID;
		[Export ("appID", ArgumentSemantic.Strong)]
		string AppID { get; set; }

		// @property (nonatomic, strong) NSString * appKey;
		[Export ("appKey", ArgumentSemantic.Strong)]
		string AppKey { get; set; }

		// @property (nonatomic, strong) NSString * domain;
		[Export ("domain", ArgumentSemantic.Strong)]
		string Domain { get; set; }

		// @property (nonatomic, strong) NSString * themeName;
		[Export ("themeName", ArgumentSemantic.Strong)]
		string ThemeName { get; set; }

		// @property (nonatomic, strong) NSString * stringsBundle;
		[Export ("stringsBundle", ArgumentSemantic.Strong)]
		string StringsBundle { get; set; }

		// @property (assign, nonatomic) BOOL gallerySelectionEnabled;
		[Export ("gallerySelectionEnabled")]
		bool GallerySelectionEnabled { get; set; }

		// @property (assign, nonatomic) BOOL cameraCaptureEnabled;
		[Export ("cameraCaptureEnabled")]
		bool CameraCaptureEnabled { get; set; }

		// @property (assign, nonatomic) BOOL notificationSoundEnabled;
		[Export ("notificationSoundEnabled")]
		bool NotificationSoundEnabled { get; set; }

		// @property (assign, nonatomic) BOOL teamMemberInfoVisible;
		[Export ("teamMemberInfoVisible")]
		bool TeamMemberInfoVisible { get; set; }

		// @property (assign, nonatomic) BOOL showNotificationBanner;
		[Export ("showNotificationBanner")]
		bool ShowNotificationBanner { get; set; }

		// -(instancetype)initWithAppID:(NSString *)appID andAppKey:(NSString *)appKey;
		[Export ("initWithAppID:andAppKey:")]
		IntPtr Constructor (string appID, string appKey);
	}

	// @interface Freshchat : NSObject
	[BaseType (typeof(NSObject))]
	interface Freshchat
	{
		// @property (readonly, nonatomic, strong) FreshchatConfig * config;
		[Export ("config", ArgumentSemantic.Strong)]
		FreshchatConfig Config { get; }

		// +(NSString *)SDKVersion;
		[Static]
		[Export ("SDKVersion")]
		//[Verify (MethodToProperty)]
		string SDKVersion { get; }

		// +(instancetype)sharedInstance;
		[Static]
		[Export ("sharedInstance")]
		Freshchat SharedInstance ();

		// -(void)initWithConfig:(FreshchatConfig *)config;
		[Export ("initWithConfig:")]
		void InitWithConfig (FreshchatConfig config);

		// -(void)showConversations:(UIViewController *)controller;
		[Export ("showConversations:")]
		void ShowConversations (UIViewController controller);

		// -(void)showConversations:(UIViewController *)controller withOptions:(ConversationOptions *)options;
		[Export ("showConversations:withOptions:")]
		void ShowConversations (UIViewController controller, ConversationOptions options);

		// -(void)showFAQs:(UIViewController *)controller;
		[Export ("showFAQs:")]
		void ShowFAQs (UIViewController controller);

		// -(void)showFAQs:(UIViewController *)controller withOptions:(FAQOptions *)options;
		[Export ("showFAQs:withOptions:")]
		void ShowFAQs (UIViewController controller, FAQOptions options);

		// -(void)setUser:(FreshchatUser *)user;
		[Export ("setUser:")]
		void SetUser (FreshchatUser user);

		// -(void)identifyUserWithExternalID:(NSString *)externalID restoreID:(NSString *)restoreID;
		[Export ("identifyUserWithExternalID:restoreID:")]
		void IdentifyUserWithExternalID (string externalID, string restoreID);

		// -(void)resetUserWithCompletion:(void (^)())completion;
		[Export ("resetUserWithCompletion:")]
		void ResetUserWithCompletion (Action completion);

		// -(void)setUserProperties:(NSDictionary *)props;
		[Export ("setUserProperties:")]
		void SetUserProperties (NSDictionary props);

		// -(void)setUserPropertyforKey:(NSString *)key withValue:(NSString *)value;
		[Export ("setUserPropertyforKey:withValue:")]
		void SetUserPropertyforKey (string key, string value);

		// -(void)setPushRegistrationToken:(NSData *)deviceToken;
		[Export ("setPushRegistrationToken:")]
		void SetPushRegistrationToken (NSData deviceToken);

		// -(BOOL)isFreshchatNotification:(NSDictionary *)info;
		[Export ("isFreshchatNotification:")]
		bool IsFreshchatNotification (NSDictionary info);

		// -(void)handleRemoteNotification:(NSDictionary *)info andAppstate:(UIApplicationState)appState;
		[Export ("handleRemoteNotification:andAppstate:")]
		void HandleRemoteNotification (NSDictionary info, UIApplicationState appState);

		// -(UIViewController *)getFAQsControllerForEmbed;
		[Export ("getFAQsControllerForEmbed")]
		//[Verify (MethodToProperty)]
		UIViewController FAQsControllerForEmbed { get; }

		// -(UIViewController *)getFAQsControllerForEmbedWithOptions:(FAQOptions *)faqOptions;
		[Export ("getFAQsControllerForEmbedWithOptions:")]
		UIViewController GetFAQsControllerForEmbedWithOptions (FAQOptions faqOptions);

		// -(UIViewController *)getConversationsControllerForEmbed;
		[Export ("getConversationsControllerForEmbed")]
		//[Verify (MethodToProperty)]
		UIViewController ConversationsControllerForEmbed { get; }

		// -(UIViewController *)getConversationsControllerForEmbedWithOptions:(ConversationOptions *)convOptions;
		[Export ("getConversationsControllerForEmbedWithOptions:")]
		UIViewController GetConversationsControllerForEmbedWithOptions (ConversationOptions convOptions);

		// -(void)unreadCountWithCompletion:(void (^)(NSInteger))completion;
		[Export ("unreadCountWithCompletion:")]
		void UnreadCountWithCompletion (Action<nint> completion);

		// -(void)unreadCountForTags:(NSArray *)tags withCompletion:(void (^)(NSInteger))completion;
		[Export ("unreadCountForTags:withCompletion:")]
		//Verify (StronglyTypedNSArray)]
		void UnreadCountForTags (NSObject[] tags, Action<nint> completion);

		// -(void)updateConversationBannerMessage:(NSString *)message;
		[Export ("updateConversationBannerMessage:")]
		void UpdateConversationBannerMessage (string message);

		// -(void)sendMessage:(FreshchatMessage *)messageObject;
		[Export ("sendMessage:")]
		void SendMessage (FreshchatMessage messageObject);

		// -(void)dismissFreshchatViews;
		[Export ("dismissFreshchatViews")]
		void DismissFreshchatViews ();
	}

	// @interface FreshchatUser : NSObject
	[BaseType (typeof(NSObject))]
	interface FreshchatUser
	{
		// @property (nonatomic, strong) NSString * firstName;
		[Export ("firstName", ArgumentSemantic.Strong)]
		string FirstName { get; set; }

		// @property (nonatomic, strong) NSString * lastName;
		[Export ("lastName", ArgumentSemantic.Strong)]
		string LastName { get; set; }

		// @property (nonatomic, strong) NSString * email;
		[Export ("email", ArgumentSemantic.Strong)]
		string Email { get; set; }

		// @property (nonatomic, strong) NSString * phoneNumber;
		[Export ("phoneNumber", ArgumentSemantic.Strong)]
		string PhoneNumber { get; set; }

		// @property (nonatomic, strong) NSString * phoneCountryCode;
		[Export ("phoneCountryCode", ArgumentSemantic.Strong)]
		string PhoneCountryCode { get; set; }

		// @property (readonly, nonatomic, strong) NSString * externalID;
		[Export ("externalID", ArgumentSemantic.Strong)]
		string ExternalID { get; }

		// @property (readonly, nonatomic, strong) NSString * restoreID;
		[Export ("restoreID", ArgumentSemantic.Strong)]
		string RestoreID { get; }

		// +(instancetype)sharedInstance;
		[Static]
		[Export ("sharedInstance")]
		FreshchatUser SharedInstance ();
	}

	// @interface FreshchatOptions : NSObject
	[BaseType (typeof(NSObject))]
	interface FreshchatOptions
	{
	}

	// @interface FAQOptions : FreshchatOptions
	[BaseType (typeof(FreshchatOptions))]
	interface FAQOptions
	{
		// @property (nonatomic) BOOL showFaqCategoriesAsGrid;
		[Export ("showFaqCategoriesAsGrid")]
		bool ShowFaqCategoriesAsGrid { get; set; }

		// @property (nonatomic) BOOL showContactUsOnFaqScreens;
		[Export ("showContactUsOnFaqScreens")]
		bool ShowContactUsOnFaqScreens { get; set; }

		// @property (nonatomic) BOOL showContactUsOnAppBar;
		[Export ("showContactUsOnAppBar")]
		bool ShowContactUsOnAppBar { get; set; }

		// -(void)filterByTags:(NSArray *)tags withTitle:(NSString *)title andType:(enum TagFilterType)type;
		[Export ("filterByTags:withTitle:andType:")]
		//[Verify (StronglyTypedNSArray)]
		void FilterByTags (NSObject[] tags, string title, TagFilterType type);

		// -(void)filterContactUsByTags:(NSArray *)tags withTitle:(NSString *)title;
		[Export ("filterContactUsByTags:withTitle:")]
		//[Verify (StronglyTypedNSArray)]
		void FilterContactUsByTags (NSObject[] tags, string title);

		// -(NSString *)filteredViewTitle;
		[Export ("filteredViewTitle")]
		//[Verify (MethodToProperty)]
		string FilteredViewTitle { get; }

		// -(NSArray *)tags;
		[Export ("tags")]
		//[Verify (MethodToProperty), Verify (StronglyTypedNSArray)]
		NSObject[] Tags { get; }

		// -(enum TagFilterType)filteredType;
		[Export ("filteredType")]
		//[Verify (MethodToProperty)]
		TagFilterType FilteredType { get; }

		// -(NSArray *)contactUsTags;
		[Export ("contactUsTags")]
		//[Verify (MethodToProperty), Verify (StronglyTypedNSArray)]
		NSObject[] ContactUsTags { get; }

		// -(NSString *)contactUsTitle;
		[Export ("contactUsTitle")]
		//[Verify (MethodToProperty)]
		string ContactUsTitle { get; }
	}

	// @interface ConversationOptions : FreshchatOptions
	[BaseType (typeof(FreshchatOptions))]
	interface ConversationOptions
	{
		// -(void)filterByTags:(NSArray *)tags withTitle:(NSString *)title;
		[Export ("filterByTags:withTitle:")]
		//[Verify (StronglyTypedNSArray)]
		void FilterByTags (NSObject[] tags, string title);

		// -(NSString *)filteredViewTitle;
		[Export ("filteredViewTitle")]
		//[Verify (MethodToProperty)]
		string FilteredViewTitle { get; }

		// -(NSArray *)tags;
		[Export ("tags")]
		//[Verify (MethodToProperty), Verify (StronglyTypedNSArray)]
		NSObject[] Tags { get; }
	}

	// @interface FreshchatMessage : NSObject
	[BaseType (typeof(NSObject))]
	interface FreshchatMessage
	{
		// @property (nonatomic, strong) NSString * message;
		[Export ("message", ArgumentSemantic.Strong)]
		string Message { get; set; }

		// @property (nonatomic, strong) NSString * tag;
		[Export ("tag", ArgumentSemantic.Strong)]
		string Tag { get; set; }

		// -(instancetype)initWithMessage:(NSString *)message andTag:(NSString *)tag;
		[Export ("initWithMessage:andTag:")]
		IntPtr Constructor (string message, string tag);
	}
}
