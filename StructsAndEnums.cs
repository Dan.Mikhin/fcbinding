using System;

namespace FreshchatSDK
{
	public enum TagFilterType : uint
	{
		Article = 1,
		Category = 2
	}
}
